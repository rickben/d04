#include <stdio.h>

int ft_iterative_factorial(int nb)
{
	if (nb < 0) return(0);

	int sum;

	sum = 1;
	if(nb <= 1) return sum;
	while(nb > 1)
	{
		sum *= nb;
		nb--;
	}
	
	return(sum);
}

int main(int argc, char const *argv[])
{
	printf("%i\n", ft_iterative_factorial(5));
	return 0;
}