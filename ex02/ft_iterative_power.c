#include <stdio.h>

int ft_iterative_power(int nb, int power)
{
	if (power < 0) return(0);

	int sum;
	sum = 1;

	int i;
	i = 0;

	while(i < power)
	{
		sum = nb * sum;
		power--;
	}
	return (sum);
}

int main(int argc, char const *argv[])
{
	printf("%i\n", ft_iterative_power(4,3));
	return 0;
}